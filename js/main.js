"use strict"; // only for dev todo: delete that for prod!

// default obj
var defaultTask = {
  DATA: [
    {
      headline: 'You haven\'t any tasks in the pipe',
      description: 'Create a new Task'
    }
  ]
};

var createTaskText = 'Create a Task';
var deleteTaskText = 'Delete Task';

// create xhr object
var loadJSON = function (param) {
  var source = './mock/data.json';
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
      if (xmlhttp.status == 200) {
        // document.getElementById('app').innerHTML = xmlhttp.responseText; // write content into div
        var getContent = xmlhttp.responseText; // write content into a variable
        var parsedJSON = JSON.parse(getContent); // create an obj

        createTasks(parsedJSON);
      }
      else if (xmlhttp.status == 400) {
        throw 'ajax call error 400';
      }
      else {
        throw 'something else other than 200 was returned';
      }
    }
  };

  xmlhttp.open("GET", source, true);
  xmlhttp.send();

};

loadJSON();

function createTasks(tasks) {

  // if any task doesn't exists
  if (Object.keys(tasks).length === 0) {
    tasks = defaultTask;
  }

  // iterate JSON
  for (var i = 0; i < tasks.DATA.length; i++) {

    // create tasks
    var headlineText = tasks.DATA[i].headline;
    var descriptionText = tasks.DATA[i].description;

    var createContainer = document.createElement('div');
    var createHeadline = document.createElement('h1');
    var createContent = document.createElement('p');

    var insertHeadlineText = document.createTextNode(headlineText);
    var insertDescriptionText = document.createTextNode(descriptionText);

    createContainer.appendChild(createHeadline);
    createContainer.appendChild(createContent);
    createContainer.classList.add('task');
    createHeadline.appendChild(insertHeadlineText);
    createContent.appendChild(insertDescriptionText);

    // write them into app div
    document.getElementById('app').appendChild(createContainer);
  }

  deleteTask();
}

function deleteTask() {

  var findTasks = document.getElementsByClassName('task');

  for (var i = 0; i < findTasks.length; i++) {
    var addButton = findTasks[i];

    // create delete btn
    var createDeleteBtn = document.createElement('button');
    createDeleteBtn.innerText = deleteTaskText;

    addButton.appendChild(createDeleteBtn);

    createDeleteBtn.onclick = function () {
      var parentEl = this.closest('div.task');
      parentEl.remove();

      if (findTasks.length === 0) {
        var defaultTask = {};
        createTasks(defaultTask);
        renameButton();
      }

      // todo: send request to delete the task from server
    };
  }
}

function renameButton() {
  var tasks = document.getElementsByClassName('task')[0].children[0].innerText;
  var button = document.getElementsByTagName('button')[0];
  var defaultTaskHeadline = defaultTask.DATA[0].headline;

  if (tasks === defaultTaskHeadline) {
    button.innerText = createTaskText;

    button.onclick = function () {
      createNewTask();
    }
  }
}

function createNewTask() {
  var modal = document.getElementById('modal');
  var closeBtn = document.getElementsByClassName('close-btn')[0];

  // Fire modal
  modal.style.display = 'block';

  // Close methods
  closeBtn.onclick = function () {
    modal.style.display = 'none';
  };

  window.onclick = function (e) {
    if (e.target == modal) {
      modal.style.display = 'none';
    }
  };

  // Create form
  createForms();
}

function createForms() {

  var createTask = 'newTask';

  var modalContent = document.getElementById('modal-content');
  var createTaskForm = '<div class=\"row\">\n' +
    '        <label for=\"headline\">Headline</label>\n' +
    '        <input id=\"headline\" type=\"text\">\n' +
    '      </div>\n' +
    '      <div class=\"row\">\n' +
    '        <label for=\"description\">Description</label>\n' +
    '        <textarea id=\"description\" type=\"text\"></textarea>\n' +
    '      </div>\n' +
    '      <div class=\"row\">\n' +
    '        <button data-save=\"task\">Submit</button>\n' +
    '      </div>';

  modalContent.innerHTML = createTaskForm;

  sendform(createTask);
}

function sendform(form) {

  if (form === 'newTask') {
    var modalContent = document.getElementById('modal-content');
    var contentHeight = modalContent.offsetHeight;
    var button = document.querySelector('[data-save="task"]');

    button.onclick = function () {
      modalContent.style.height = contentHeight + 'px';
      modalContent.innerHTML = 'You created a new task';
    };
  }
}
